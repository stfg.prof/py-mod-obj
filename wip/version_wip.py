import re
import random
import datetime
import numpy as np

# import os
# import argparse
# from sys import argv

# dispatch y callbacks
# https://softwareengineering.stackexchange.com/questions/182093/why-store-a-function-inside-a-python-dictionary

# decorador muy interesanta para crear 3 clases de mat coord y gruops
# https://www.geeksforgeeks.org/dispatch-decorator-in-python/

"""
este script carga un archivo desglozandolo en un diccionario con entradascon las caracteristicas de un obj 3d
y luego permite generarle modificaciones a los mismos para luego guardar un nuevo archivo con dichas modificaciones
(el nombre del archivo sera la fecha de generacion del mismo a modo de id unico y podra pasarse un argumento para el nuevo nombre y si se desea la fecha en el mismo o no)
las modificaciones basicas seran un radom y un shuffle.

#tarda un minuto aproximadamente en procesar un archivo de 4 millones y medio de parametros
# como codificar una serie de acciones complejas por ejemplo aplicar una func a un slice y a otro slice posterior aplficar otra y luego en la general una func
# dos tipos de slice sobre los n° y otro sobre los grupos


Funcional
    Shuffle de vertices (sol lewits style :P) <--proto integrare un shuffle mas ordenado

Bugs:
    Actualmente el shufle de los planoshace ilegible el archivo
    hay archivos que contineen instrucciones de mat o g que no pueden abrirse sin reponer manualmente esas lineas principales
    archivos con // son incorrectamente procesados (ej semicomplejo) a su vez sucede lo mismo en los archivos con grupos de 2 y 3 coord por vertices
    hay saltos de linea "/" si existen el obj no se carga correctamente

ROADMAP | Proximamrnte
    Generar una pipeline en base a convertir todo a arrays de numpy (simplificar el dict datos !!!)
    Dirimir si hacer 3 tipos de datos los de posiciones como Arrays, los agrupamientos (planos, lineas puntos) y los materiales?

    DISPATCH para generar una serie de cambios segun la lista de elementos
    Generar clases metodos contructores y que el script principal componga (pensando en procesamientos paralelos en un futuro  muy lejano)
    -algoritmos de modificacion
    Construir una funcion que realice una serie ordenada de modificaciones
    reciviendo el tipo de elemento a modificar y la lista de modificadores
    que pueda recibir un max y min
    y a la ves un slice [a:b:c]
    poder aplicar filtros de kernel 
    y probar un AC sobre el array de kernel de 3X3X3 (scypy)

    -agregar variables externas | falta generar el parser de los las variables del script
        pathArchivo
        fecha
        nombre
        decidir modificadores
        estudiar y agregar todos los tipos del formato .obj y las ociones
        mejorar el ReGex para filtrar comentarios y guardarlos
        añadir unontador de lineas para interponer los mismos?
    
#ojo con esto, las texturas pueden ser int o float

por temas de tiempo y practicidad este sketch no lee archicos comentados "#"y tiene dificualtada para separar los mismos (ejemplo toma el v2.93 como un vector desde :: # Blender v2.93.2 OBJ File: '')
a su vez aun nointegre caracteristicas de materiales y opciones // usemtl Material  // s off

CONCLUSIONES:
    dividir el procesamiento en 5 tipos de datos coordenadas, grupos, materiales, comentarios y uno que quizas esta en el anterior que seria instrucciones de procesammiento
    generar 2  clases de modificadores, los que alteran y los que crean/destruyen 
        los primeros pueden ser 
            del orden de la magnitud aumentando multiplicando etc 
            o modificadores del orden de los elementos
        los del segundo tipo
             crean o destruyen elementos y los incertan 
    (quizas modificar la carga y q sea una lectura lineal y no un gran regex para protegerse de saltos de paginas)
"""

# IMPORTANTE EL TEMA DE CONVERTIR LOS ELEMENTOS A ARRAYS DE NP Y LUEGO RECONVERTIRLOS A STR | a su vez tener cuidado con los generadores de np actuales estoy usando los legacy
# REFES UTILES :
# https://gitlab.com/ayrsd/audiostellar/-/blob/units/data-analysis/doProcess.py
# https://www.w3schools.com/python/python_datetime.asp#:~:text=A%20date%20in%20Python%20is,with%20dates%20as%20date%20objects.
# https://stackoverflow.com/questions/36268749/how-to-remove-multiple-items-from-a-list-in-just-one-statement
# https://www.w3schools.com/python/python_ref_list.asp
# https://www.w3schools.com/python/python_lists_sort.asp
# https://www.w3schools.com/python/ref_func_map.asp
# https://www.w3schools.com/python/ref_func_zip.asp
# https://numpy.org/doc/stable/reference/random/generated/numpy.random.normal.html

pathArchivo = 'moebius.obj'  # 'test.obj'  # 'prueba.obj'  #
tiempoHoy = datetime.datetime.now()
fecha = tiempoHoy.strftime("%Y%M%d_%H%M%S")
nombreArchivo = "moe"


def sliceReverseReplace(lista, inicio=None, fin=None, paso=None):
    """genera un slice de una lista, lo invierte y remplaza los valores en los indices del slice inicial"""
    # hacer un slice luego un index de eso un reverse un pop o un replace element o pop + insert
    listaTemp = lista[inicio:fin:paso]
    listTemporalCopy = lista.copy()
    listTemporalCopy.reverse()
    # aca tengo que tener cuidado por las repeticiones
    for i, j in enumerate(listaTemp):
        lista[lista.index(j)] = listTemporalCopy[i]


# def slice3dAppend(axis, inicio=None, fin=None, paso=None):
#     # me falta investigar mejor numpy  la mayoria de lso metodos que uso podrian ser mas simples y explicitos
#     x = np.array([float(n) for n in datos["vertices"]["numeros"][0::3]])
#     y = np.array([float(n) for n in datos["vertices"]["numeros"][1::3]])
#     z = np.array([float(n) for n in datos["vertices"]["numeros"][2::3]])
#     if(axis == 0):
#         listaTemp = x[inicio:fin:paso]
#         listaIndex = np.array([x == listaTemp])
#         print(listaIndex)
#         print(listaIndex.shape)
#         print(x)
#         print(x.shape)
#         x = np.concatenate([x, listaTemp])
#         x = np.delete(x, listaIndex)

#     if(axis == 1):
#         listaTemp = y[inicio:fin:paso]
#         listaIndex = [y == listaTemp]
#         y = np.concatenate([y, listaTemp])
#         y = np.delete(y, listaIndex)

#     if(axis == 2):
#         listaTemp = z[inicio:fin:paso]
#         listaIndex = [z == listaTemp]
#         z = np.concatenate(z, listaTemp)
#         z = np.delete(z, listaIndex)

#         # coord3d = np.vstack([x, y, z])
#         # coord3d.flatten()
#         coord3d = np.concatenate([x, y, z])
#         datos["vertices"]["numeros"] = coord3d.tolist()


def sliceAppend(lista, inicio=None, fin=None, paso=None):
    """genera un slice de una lista, retira dichos valores y luego los agrega a lo ultimo, no esta corretamente testeada para slice en reversa"""
    listaTemp = lista[inicio:fin:paso]
    # no contemplo listas invertidar en esos index???
    listaIndex = [lista.index(l) for l in listaTemp]
    # invierto para ir sacando elementos de atras para adelante y que el index no cambie (sabiendo que el la lista de index estaria ordenada)
    # este sort es para preveer que el slice si se haceren reversa no rompa la funcion aunque puede relentizar muchisimo
    # listaIndex.sort()
    listaIndex.reverse()
    # lista.extend([lista.pop(j) for j in listaIndex]) # sliceAppend(lista, int(len(lista)/2), len(lista), 2) # se rompe
    lista.extend(listaTemp)
    for i in listaIndex:
        lista.pop(i)


def sumRand(lista, media=0, desviacion=10):  # se rompeee con vertices grandes
    # https://numpy.org/doc/stable/reference/arrays.dtypes.html
    """ Funcion solamente util para vectores!! suma un rand gauss al array"""
    # la misma func a grupos y otras cosas no es muy util
    # transformar la lsita en Array y luego a lista ?  poder hacer slices? sumRand(lista, inicio=None, fin=None, paso=None):
    print(lista)

    arrayTemp = np.array([float(n) for n in lista])
    # stringArray = np.array(lista)
    # floatArray = stringArray.astype(float)
    # arrayTemp = floatArray
    arrayRand = np.random.normal(media, desviacion, size=(len(lista)))
    arraySumas = np.add(arrayTemp, arrayRand)
    # podria hacer un return pero aun no se si es conveniente | hice estos mlabaras para pasar a n° luego a str y slice para que funcione por referencia sino era via copia
    lista[::] = [str(n) for n in arraySumas.tolist()]
    # lista[::] = [str(n) for n in arrayTemp.tolist()]
    print(lista)


def linearRand(lista):
    # estos modificadores que utilizan np seran mas utiles separando x y z
    arrayTemp = np.array([float(n) for n in lista])
    lenght = len(arrayTemp)
    # , endpoint=True, dtype=int)
    # lineal = np.linspace(1, lenght*0.0025, lenght)
    lineal = np.linspace(1./lenght, 1, lenght)
    rand = np.random.random(lenght)
    arrayRand = lineal*rand
    arraySumas = np.add(arrayTemp, arrayRand)
    lista[::] = [str(n) for n in arraySumas.tolist()]


def sliceAppendPlanos(inicio=None, fin=None, paso=None):
    datos["planos"]["numeros"].clear()
    listaTemp = datos["planos"]["subgrupos"][inicio:fin:paso]
    listaIndex = [datos["planos"]["subgrupos"].index(l) for l in listaTemp]
    listaIndex.reverse()
    datos["planos"]["subgrupos"].extend(listaTemp)
    for i in listaIndex:
        datos["planos"]["subgrupos"].pop(i)
    for subgrupo in datos["planos"]["subgrupos"]:
        numeros = re.findall(
            patrones_num[estructura["planos"]["tipo"]], subgrupo)
        for numero in numeros:
            datos["planos"]["numeros"].append(numero)


def shufflePlanos():
    # print(datos["planos"]["numeros"])
    datos["planos"]["numeros"].clear()
    random.shuffle(datos["planos"]["subgrupos"])
    # print(datos["planos"]["subgrupos"])
    for subgrupo in datos["planos"]["subgrupos"]:
        numeros = re.findall(
            patrones_num[estructura["planos"]["tipo"]], subgrupo)
        for numero in numeros:
            datos["planos"]["numeros"].append(numero)
    # print(datos["planos"]["numeros"])


dispatch = {
    'sRand': sumRand,
    'sAppend': sliceAppend,
    'sRReplace': sliceReverseReplace,
    'linearRand': linearRand
    # falta agregar func
}

# def aplicarModificador(command, ref,options):


def aplicarModificador(command, arg):
    dispatch[command](arg)

# argv =["v", "f", -out " "]
# args = ["vertices", "planos"]


# esto lo idear seria pasarlo como argv y luego el metodo con parametros de edicion desde el bash
# args = ["vertices"]
args = ["vertices"]

printear = False  # True  #


# flags = {  # aqui tambien deberia ver si puedo combinar o pasarparametros y sub modificaciones no se si llamarlo flags
#     "v": "vertices",
#          "vt": "textura",
#          "vn": "normales",
#          "vp": "parametros",
#          "f": "planos",
#          "l": "lineas"
# }

patrones_num = {
    "int": '\d+',
    "float": '[\-]*\d+\.\d+'
}
estructura = {

    "vertices": {
        "nomenclatura": "v",
        "tipo": "float",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'v(\s*-*\d.*\d)[\r\n]*'  # creo q estan demas [\r\n]
    },
    "textura": {
        "nomenclatura": "vt",
        "tipo": "int",  # ojo con esto, las texturas pueden ser int o float
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'vt(\s*-*\d.*\d)\n'
    },
    "normales": {
        "nomenclatura": "vn",
        "tipo": "float",
        "cantidadDeGrupos": 0,
        "patron": 'vn(\s*-*\d.*\d)\n'
    },
    "parametros": {
        "nomenclatura": "vp",
        "tipo": "float",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'vp(\s*-*\d.*\d)\n'
    },
    "planos": {
        "nomenclatura": "f",
        "tipo": "int",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'f(\s*\d.*)',
        # grupo provisorio, captura hasta la siguiente linea (no rompe nada por que ya esta prefiltrado)
        "subPatron": '([\d\/+]+[\s]*)[\r\n]*'
        # '([\d\/]+[\s])[\r\n]*'  # '[\d\/]+[\s]'
    },
    "lineas": {
        "nomenclatura": "l",
        "tipo": "int",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'l(\s *\d.*)'  # VOLVER A EVALUAR !!
    }
}
datos = {
    "vertices": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": [],
        "d3": []
    },
    "textura": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []

    },
    "normales": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []

    },
    "parametros": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []

    },
    "planos": {
        # los planos segu nel obj pueden ser quad o triangulares por eso varia la cantidad de subgrupos y por las dudas calculo numerosPorSubGrupos aunque seria pevisible 3
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "cantidadDeSubGrupos": 0,
        "numerosPorSubGrupos": 0,
        "lenSubgrupos": 0,  # estavariable tiene utilidad?
        "grupos": [],
        "subgrupos": [],
        "numeros": []

        # sub conjuntos ([\d\/]+[\s]) o grupos mejor dicho
    },
    "lineas": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []
    }
}
# algo importante pensar en el error por compentarios #
# SERIA MUCHO MAS EFICAZ UN SCRIPT QUE SOLO MODIFIQUE LAS N° DESEADAS! sin necesidad de leer todo el obj (tambien se podria aplicar un multithread)
try:
    # por un tema de tipos al file no puedo pasarlo dierecto por el regex
    with open(pathArchivo, 'r') as archivoLectura:
        # abro el
        lines = archivoLectura.read()
        for key in estructura.keys():

            concidencias = re.findall(
                estructura[key]["patron"], lines)
            datos[key]["grupos"] = concidencias
            datos[key]["cantidadDeGrupos"] = len(concidencias)

            for concidencia in concidencias:

                numeros = re.findall(
                    patrones_num[estructura[key]["tipo"]], concidencia)

                for d in numeros:
                    datos[key]["numeros"].append(d)

                if(key == "planos"):
                    # cantidadDeSubGrupos
                    subgrupos = re.findall(
                        estructura[key]["subPatron"], concidencia)
                    datos[key]["cantidadDeSubGrupos"] = len(subgrupos)
                    datos[key]["numerosPorSubGrupos"] = len(
                        re.findall(patrones_num[estructura[key]["tipo"]], subgrupos[0]))
                    for subgrupo in subgrupos:
                        datos[key]["subgrupos"].append(subgrupo)
# no encontre mejor metafora para los grupos de 12 numeros compuestos por 4 grupos de 3 numeros
                    datos[key]["cantidadNumerosPorGrupos"] = datos[key]["numerosPorSubGrupos"] * \
                        datos[key]["cantidadDeSubGrupos"]
                    datos[key]["lenSubgrupos"] = len(
                        datos[key]["subgrupos"])
                else:
                    datos[key]["cantidadNumerosPorGrupos"] = len(numeros)
            datos[key]["cantidadTotalNumeros"] = len(datos[key]["numeros"])

# hacer un zip o un map ?? o un for anidado segun args y los mod de cada argv
    # modificadores = ['linearRand', 'sAppend', 'linearRand', 'sRReplace']
    # for key in args:
    #     # aplicarModificador(
    #     #     'linearRand', datos[key]["numeros"])
    #     for mod in modificadores:
    #         aplicarModificador(mod, datos[key]["numeros"])
    # shufflePlanos()

    sliceAppendPlanos(0, int(datos["planos"]["lenSubgrupos"]/3),
                      3)
    # slice3dAppend(0, 0, int(datos["planos"]["lenSubgrupos"]/3),
    #               3)
# testeos


# BUCLE IMPRIMIR CONTENIDO (DEBUG)
    if(printear):
        for key in estructura.keys():
            print(datos[key])
            print("\n\n")
    # tendria que recontruir el archivo desde sus grupos
    with open(nombreArchivo + "_" + fecha+'.obj', 'w') as archivoEscritura:
        for key in estructura.keys():
            if(datos[key]["cantidadDeGrupos"] > 0):
                for i, n in enumerate(datos[key]["numeros"]):
                    # funciona pero algo no me cierra
                    if(key == "planos"):
                        # aca añadir un if por el tipo de modificacion?
                        if(i % datos[key]["numerosPorSubGrupos"] == 0):
                            archivoEscritura.write(" ")
                        else:
                            archivoEscritura.write("/")
                        if(i % datos[key]["cantidadNumerosPorGrupos"] == 0):
                            # no lo puedo calcular pero la intuicion y la practica me dicen que corto el gurpo imprimiendo
                            archivoEscritura.write("\n")
                            archivoEscritura.write(
                                estructura[key]["nomenclatura"])
                            archivoEscritura.write(" ")
                        archivoEscritura.write(n)
                    else:
                        if(i % datos[key]["cantidadNumerosPorGrupos"] == 0):
                            archivoEscritura.write("\n")
                            archivoEscritura.write(
                                estructura[key]["nomenclatura"])
                        archivoEscritura.write(" ")
                        archivoEscritura.write(n)
#   PRINT DE LOS DATOS YA CARGADOS (sin modificacion)


except FileNotFoundError:
    print("The 'docs' directory does not exist")
