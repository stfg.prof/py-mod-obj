import re
import datetime
import argparse

parser = argparse.ArgumentParser()
# actualmente el slicer solo funciona con numeros naturales
parser.add_argument("--objetoPath", default="cubo.obj", help="Path to del objeto")
parser.add_argument("--start", default=0, help="inicio del slice")
parser.add_argument("--step", default=1, help="pasos del slice")
parser.add_argument("--end", default=-1, help="limite del slice")
parser.add_argument("--out", default="cubo", help="nombre del objeto procesado")
parser.add_argument("--imprimirfecha", default="True", help="pasos del slice")


args = parser.parse_args()


start = int(args.start)
end = int(args.end)
step = int(args.step)
pathArchivo = args.objetoPath  # 'moebius.obj'  # 'test.obj'  # 'prueba.obj'  #
tiempoHoy = datetime.datetime.now()

nombreArchivo = "out/"+args.out

if(args.imprimirfecha == "true" or args.imprimirfecha == "True"):
    fecha = tiempoHoy.strftime("%Y%M%d_%H%M%S")
    nombreArchivo = nombreArchivo + "_" + fecha+'.obj'
else:
    nombreArchivo = nombreArchivo + '.obj'


def sliceAppendPlanos(inicio=None, fin=None, paso=None):
    """sliceAppend retira n° planos y los incerta al final del objeto"""
    datos["planos"]["numeros"].clear()
    listaTemp = datos["planos"]["subgrupos"][inicio:fin:paso]
    listaIndex = [datos["planos"]["subgrupos"].index(l) for l in listaTemp]
    listaIndex.reverse()
    datos["planos"]["subgrupos"].extend(listaTemp)
    for i in listaIndex:
        datos["planos"]["subgrupos"].pop(i)
    for subgrupo in datos["planos"]["subgrupos"]:
        numeros = re.findall(
            patrones_num[estructura["planos"]["tipo"]], subgrupo)
        for numero in numeros:
            datos["planos"]["numeros"].append(numero)


printear = False  # True  #

patrones_num = {
    "int": '\d+',
    "float": '[\-]*\d+\.\d+'
}
estructura = {

    "vertices": {
        "nomenclatura": "v",
        "tipo": "float",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'v(\s*-*\d.*\d)[\r\n]*'  # creo q estan demas [\r\n]
    },
    "textura": {
        "nomenclatura": "vt",
        "tipo": "int",  # ojo con esto, las texturas pueden ser int o float
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'vt(\s*-*\d.*\d)\n'
    },
    "normales": {
        "nomenclatura": "vn",
        "tipo": "float",
        "cantidadDeGrupos": 0,
        "patron": 'vn(\s*-*\d.*\d)\n'
    },
    "parametros": {
        "nomenclatura": "vp",
        "tipo": "float",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'vp(\s*-*\d.*\d)\n'
    },
    "planos": {
        "nomenclatura": "f",
        "tipo": "int",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'f(\s*\d.*)',
        # grupo provisorio, captura hasta la siguiente linea (no rompe nada por que ya esta prefiltrado)
        "subPatron": '([\d\/+]+[\s]*)[\r\n]*'
        # '([\d\/]+[\s])[\r\n]*'  # '[\d\/]+[\s]'
    },
    "lineas": {
        "nomenclatura": "l",
        "tipo": "int",
        "cantidadDeGrupos": 0,
        "grupos": [],
        "patron": 'l(\s *\d.*)'  # VOLVER A EVALUAR !!
    }
}
datos = {
    "vertices": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": [],
        "d3": []
    },
    "textura": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []

    },
    "normales": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []

    },
    "parametros": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []

    },
    "planos": {
        # los planos segu nel obj pueden ser quad o triangulares por eso varia la cantidad de subgrupos y por las dudas calculo numerosPorSubGrupos aunque seria pevisible 3
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "cantidadDeSubGrupos": 0,
        "numerosPorSubGrupos": 0,
        "lenSubgrupos": 0,  # estavariable tiene utilidad?
        "grupos": [],
        "subgrupos": [],
        "numeros": []

        # sub conjuntos ([\d\/]+[\s]) o grupos mejor dicho
    },
    "lineas": {
        "cantidadDeGrupos": 0,
        "cantidadNumerosPorGrupos": 0,
        "cantidadTotalNumeros": 0,
        "grupos": [],
        "numeros": []
    }
}

# algo importante pensar en el error por compentarios #
try:
    # por un tema de tipos al file no puedo pasarlo dierecto por el regex
    with open(pathArchivo, 'r') as archivoLectura:
        # abro el
        lines = archivoLectura.read()
        for key in estructura.keys():

            concidencias = re.findall(
                estructura[key]["patron"], lines)
            datos[key]["grupos"] = concidencias
            datos[key]["cantidadDeGrupos"] = len(concidencias)

            for concidencia in concidencias:

                numeros = re.findall(
                    patrones_num[estructura[key]["tipo"]], concidencia)

                for d in numeros:
                    datos[key]["numeros"].append(d)

                if(key == "planos"):
                    # cantidadDeSubGrupos
                    subgrupos = re.findall(
                        estructura[key]["subPatron"], concidencia)
                    datos[key]["cantidadDeSubGrupos"] = len(subgrupos)
                    datos[key]["numerosPorSubGrupos"] = len(
                        re.findall(patrones_num[estructura[key]["tipo"]], subgrupos[0]))
                    for subgrupo in subgrupos:
                        datos[key]["subgrupos"].append(subgrupo)
# no encontre mejor metafora para los grupos de 12 numeros compuestos por 4 grupos de 3 numeros
                    datos[key]["cantidadNumerosPorGrupos"] = datos[key]["numerosPorSubGrupos"] * \
                        datos[key]["cantidadDeSubGrupos"]
                    datos[key]["lenSubgrupos"] = len(
                        datos[key]["subgrupos"])
                else:
                    datos[key]["cantidadNumerosPorGrupos"] = len(numeros)
            datos[key]["cantidadTotalNumeros"] = len(datos[key]["numeros"])

    if(end < 0):
        end = datos["planos"]["lenSubgrupos"]

    sliceAppendPlanos(start, end, step)

    with open(nombreArchivo, 'w') as archivoEscritura:
        for key in estructura.keys():
            if(datos[key]["cantidadDeGrupos"] > 0):
                for i, n in enumerate(datos[key]["numeros"]):
                    if(key == "planos"):
                        if(i % datos[key]["numerosPorSubGrupos"] == 0):
                            archivoEscritura.write(" ")
                        else:
                            archivoEscritura.write("/")
                        if(i % datos[key]["cantidadNumerosPorGrupos"] == 0):
                            # no lo puedo calcular pero la intuicion y la practica me dicen que corto el gurpo imprimiendo
                            archivoEscritura.write("\n")
                            archivoEscritura.write(
                                estructura[key]["nomenclatura"])
                            archivoEscritura.write(" ")
                        archivoEscritura.write(n)
                    else:
                        if(i % datos[key]["cantidadNumerosPorGrupos"] == 0):
                            archivoEscritura.write("\n")
                            archivoEscritura.write(
                                estructura[key]["nomenclatura"])
                        archivoEscritura.write(" ")
                        archivoEscritura.write(n)

except FileNotFoundError:
    print("a cocurrido algun error en la carga del archivo")
